package com.disney.studios;

import org.slf4j.Logger;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.Resource;
import org.springframework.stereotype.Component;

import com.disney.studios.dto.DogBreed;

import javax.sql.DataSource;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;

import static org.slf4j.LoggerFactory.getLogger;
import org.springframework.context.annotation.Bean;

/**
 * @author mb627b
 *
 */
@Component
public class PetLoader implements InitializingBean {

    private static final Logger LOGGER = getLogger(PetLoader.class);

    // Resources to the different files we need to load.
    @Value("classpath:data/labrador.txt")
    private Resource labradors;

    @Value("classpath:data/pug.txt")
    private Resource pugs;

    @Value("classpath:data/retriever.txt")
    private Resource retrievers;

    @Value("classpath:data/yorkie.txt")
    private Resource yorkies;

    @Autowired
    DataSource dataSource;
    
    /**
     * Load the different breeds into the data source after
     * the application is ready.
     *
     * @throws Exception In case something goes wrong while we load the breeds.
     */
    @Override
    public void afterPropertiesSet() throws Exception {
        loadBreed("Labrador", labradors);
        loadBreed("Pug", pugs);
        loadBreed("Retriever", retrievers);
        loadBreed("Yorkie", yorkies);
    }
    
    /**
     * Reads the list of dogs in a category and (eventually) add
     * them to the data source.
     * @param breed The breed that we are loading.
     * @param source The file holding the breeds.
     * @throws IOException In case things go horribly, horribly wrong.
     */
    private void loadBreed(String breed, Resource source) throws IOException {
        LOGGER.debug("Loading breed {}", breed);
        JdbcTemplate template = jdbcTemplate();
        template.update("INSERT INTO DOG_BREED (BREED_NAME) VALUES (?)", breed);
        DogBreed dogBreedDB = template.queryForObject("select * from DOG_BREED WHERE BREED_NAME = ?1",
	            new Object[] { breed }, new BeanPropertyRowMapper<>(DogBreed.class));
        try ( BufferedReader br = new BufferedReader(new InputStreamReader(source.getInputStream()))) {
            String line;                       
            while ((line = br.readLine()) != null) {
                LOGGER.debug(line);                
                String identity = line.substring(line.length()-11, line.length()-4);
                template.update("INSERT INTO DOG_IMAGE (IMAGE_IDENTITY, IMAGE_URL, VOTE, BREED_ID) VALUES (?, ?, ?, ?)",
            		   identity, line, 0, dogBreedDB.getBreed_id());
            }
        }
    }   
    
    
    /** jdbcTemplate to execute the query
     * Get the JDBC Template
     * @return
     */
    @Bean
    public JdbcTemplate jdbcTemplate() {
        JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSource);
        jdbcTemplate.setResultsMapCaseInsensitive(true);
        return jdbcTemplate;
    }
}
