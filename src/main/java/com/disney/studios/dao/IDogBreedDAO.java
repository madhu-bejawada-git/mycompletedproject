package com.disney.studios.dao;

import java.util.List;

import com.disney.studios.dto.DogBreed;
import com.disney.studios.dto.DogImage;


public interface IDogBreedDAO {
	/**
	 * @return
	 */
	List<DogBreed> getAllDogBreeds();
	
	/**
	 * @param breed
	 * @return
	 */
	DogBreed getDogBreedByName(String breed);
	
	/**
	 * @param dogImage
	 */
	void updateDogBreed(DogImage dogImage);
	
	/**
	 * @param identity
	 * @return
	 */
	DogImage getDogImageByIdentity(String identity);
}
