/**
 * 
 */
package com.disney.studios.service.rs;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import org.springframework.web.bind.annotation.RequestBody;
import com.disney.studios.dto.DogImage;
/**
 * @author Madhu
 *
 */
@Path("/service")
@Produces({MediaType.APPLICATION_JSON})
public interface IDogBreedController {
	
	/**
	 * List all of the available dog pictures grouped by breed
	 * @return Response have list of DogBreeds with Dog images
	 */
	@GET
	@Produces({ MediaType.APPLICATION_JSON})
	@Path("/dogbreeds")
	public Response getAllDogBreeds();
	
	
	/**
	 * List all of the available dog pictures of a particular breed
	 * @param name Dog Breed Name
	 * @return Response object contain DogBreed with DogImage
	 */
	@GET
	@Produces({ MediaType.APPLICATION_JSON})
	@Path("/dogbreed/{name}")
	public Response getDogBreedByName(@PathParam("name") String name);
	
	/**
	 * Vote up and down a dog picture
	 * @param dogImage the image identity and vote to find and update the image vote
	 * @return Response success or fail
	 */
	@POST
	@Consumes({ MediaType.APPLICATION_JSON })
    @Produces({ MediaType.APPLICATION_JSON })
	@Path("/votedogimage")
	public Response voteDogImage(@RequestBody DogImage dogImage);
	
	/**
	 * Get image meta data for a given image identity
	 * @param imageIdentity Dog Image Identity
	 * @return Response object contain DogImage
	 */
	@GET
	@Produces({ MediaType.APPLICATION_JSON})
	@Path("/dogimage/{imageIdentity}")
	public Response getDogImageByIdentity(@PathParam("imageIdentity") String imageIdentity);
	
	
}
