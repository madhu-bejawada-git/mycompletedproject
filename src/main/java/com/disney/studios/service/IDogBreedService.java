package com.disney.studios.service;

import java.util.List;

import com.disney.studios.dto.DogBreed;
import com.disney.studios.dto.DogImage;


public interface IDogBreedService {
	/**
	 * @return List of DogBreeds
	 */
	List<DogBreed> getAllDogBreeds();
	
	/**
	 * @param breed
	 * @return DogBreed
	 */
	DogBreed getDogBreedByName(String breed);
	
	/**
	 * update vote
	 * @param dogImage
	 */
	void updateDogBreed(DogImage dogImage);
	
	/**
	 * @param imageByIdentity
	 * @return DogImage
	 */
	DogImage getDogImageByIdentity(String imageByIdentity);
}
